# PFWD Minimal HUD
An Arma 3 mod that allows players to play infantry without vanilla HUD and only get some information on request (ammo estimate, fire mode and zeroing).

## Dependencies
CBA_A3

## Usage
Player has to create 3 keybinds - it is recommended to use Custom Actions from Vanilla and bind those in CBA menu.
Actions:
 - **Check Ammo**
 - **Check Fire Mode**
 - **Check Zeroing**

### Check Ammo
A trained soldier should be able to make a rough estimate of ammo left in a magazine based on weight and other factors.
To emulate this, there are following ammo states:
 - **Empty:** when there is no ammo left in the magazine.
 - **Almost Empty:** between 0% and 20% of ammo left.
 - **Less than 1/2:** between 20% and 40% of ammo left.
 - **Roughly 1/2:** between 40% and 60% of ammo left.
 - **More than 1/2:** between 60% and 80% of ammo left.
 - **Almost Full:** between 80% and 100% of ammo left.
 - **Full:** Magazine is completely full.

### Check Fire Mode
Sadly many weapons, vanilla and modded do not have fire mode switch where player can visually inspect it and even if they do it's rather cumbersome.
With this action player checks current fire mode of the weapon.

### Check Zeroing
Similar to above, this displays current zeroing value of player's weapon.
