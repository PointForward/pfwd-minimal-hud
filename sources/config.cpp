class CfgPatches
{
	class PFWD_MinimalHUD
	{
		name="Minimal HUD";
		requiredVersion = 0.1;
		author = "PointForward";
		version = 0.0.1;

		weapons[] = {};
		units[] = {};
		requiredAddons[] = {
			"cba_keybinding",
			"cba_main",
			"cba_main_a3",
			"cba_settings",
			"extended_eventhandlers"
		};
	};
};

class Extended_PreInit_EventHandlers
{
	class PFWD_MinimalHUD
	{
		init="call compile preprocessFileLineNumbers 'PFWD_MinimalHUD\XEH_PreInit.sqf'";
	};
};