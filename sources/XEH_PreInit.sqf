// Register a simple keypress to an action
#include "\a3\editor_f\Data\Scripts\dikCodes.h"

[
"PFWD_MinimalHUD", 
"PFWD_CheckFireMode", 
["Check Fire Mode", "Press to display your current fire mode"], 
{
execVM "\PFWD_MinimalHUD\checkFireMode.sqf"
}, 
{}, 
[DIK, [false,false,false]]
] call cba_fnc_addKeybind;

[
"PFWD_MinimalHUD", 
"PFWD_CheckAmmo", 
["Check Ammo", "Press to display your current ammo"], 
{
execVM "\PFWD_MinimalHUD\checkAmmo.sqf"
}, 
{}, 
[DIK, [false,false,false]]
] call cba_fnc_addKeybind;

[
"PFWD_MinimalHUD", 
"PFWD_CheckZeroing", 
["Check Zeroing", "Press to display your current zeroing"], 
{
execVM "\PFWD_MinimalHUD\checkZeroing.sqf"
}, 
{}, 
[DIK, [false,false,false]]
] call cba_fnc_addKeybind;