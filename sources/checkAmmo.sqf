[] spawn {

  _currentMag = currentMagazineDetail player;
  _one = _currentMag splitString "(";
  _two = _one select 1;
  _three = _two splitString ")";
  _four = _three select 0;
  _five = _four splitString "/";

  _currentAmmo = parseNumber (_five select 0);
  _maxAmmo = parseNumber (_five select 1);

  if (_maxAmmo != 0) then {

    _percentage = _currentAmmo/_maxAmmo;

    if (_percentage == 1) then {
      hint "Ammo: Full";
    } else {
      if (_percentage > 0.8) then {
        hint "Ammo: Almost Full";
      } else {
        if (_percentage > 0.6) then {
          hint "Ammo: More than 1/2";
        } else {
          if (_percentage > 0.4) then {
          hint "Ammo: Roughly 1/2";
          } else {
            if (_percentage > 0.2) then {
              hint "Ammo: Less than 1/2";
            } else {
              if (_percentage > 0) then {
                hint "Ammo: Almost Empty";
              } else {
                hint "Ammo: Empty";
              };
            };
          };
        };
      };
    };
  };
};